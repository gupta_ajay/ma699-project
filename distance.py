import math
import pandas as pd
import numpy as np

def sim(x,y):
	[a,b,c,d] = [0,0,0,0]
	for k in range(len(x)):
		if x[k] == 1 and y[k] == 1:
			a += 1
		if x[k] == 0 and y[k] == 1:
			b += 1
		if x[k] == 1 and y[k] == 0:
			c += 1
		if x[k] == 0 and y[k] == 0:
			d += 1
			
	return 1-(a/(a+b)+a/(a+c)+d/(b+d)+d/(c+d))/4		#For High & moderately sensitive fields
	# return 1 - 2*(a+d)/(2*a+b+c+2*d)					#For Low sensitive fields


#ExpiryTime
#Severity
#Reliability
my_csv = pd.read_csv('Benign.csv')
column = my_csv.Reliability			#Select field column of benign.csv
nstr = list(column[763:773])		#Select range of column
dim = 10							#Dimension of column-range
# print(nstr)
A = []
for i in range(dim):
	mstr = str(nstr[i]).zfill(7)
	entry = list(map(int, mstr))
	A.append(entry)
my_csv2 = pd.read_csv('Malicious_used.csv')
column2 = my_csv2.Reliability			#Select field column of benign.csv
nstr2 = list(column2[35:40])			#Select range of column
for i in range(5):
	mstr2 = str(nstr2[i]).zfill(7)
	entry = list(map(int, mstr2))
	A.append(entry)

# print(A)
# print("Distance Score")
B = []
for i in range(dim+5):
	r = []
	for j in range(dim+5):
		if j == i:
			r.append(0)
		if j < i:
			r.append(sim(A[j],A[i]))
		if j > i:
			r.append(sim(A[i], A[j]))
	B.append(r)

#print distance matrix
for i in range(dim+5): 
    for j in range(dim+5):
    	if j <= i: 
        	print("%4.2f" %B[j][i], end = " ") 
    print()

#to find range of distance matrix
# print(np.amax(B), np.amin(B+np.identity(dim)))
